import boto3
from nextflow_runner import dynamodb_functions
from flask_login import current_user

PROFILE_NAME = 'test'
REGION_NAME = 'eu-west-2'
SQS_QUEUE = 'https://sqs.eu-west-2.amazonaws.com/123456789/nextflow_jobs'

def submit_job_to_sqs(workflow, data_path, reference_path = None, abricate_database = None):
    session = boto3.Session(profile_name=PROFILE_NAME)
    sqs = session.resource('sqs', region_name = REGION_NAME )
    queue = sqs.Queue(SQS_QUEUE)

    user_workflows = dynamodb_functions.get_user_workflows(current_user.email)
    matching_workflows = [user_workflow for user_workflow in user_workflows if user_workflow['workflow'] == workflow and user_workflow['data_path'] == 's3://{0}/fastqs'.format(data_path)]
    if len(matching_workflows) > 0:
        response = {'status': 'duplicated_submission'}
    else:
        message_attributes = {
            'workflow': {
                'StringValue': workflow,
                'DataType': 'String'
            },
            'data_path': {
                'StringValue': data_path,
                'DataType': 'String'
            }
        }
        if reference_path:
            message_attributes['reference_path'] = {
                'StringValue': reference_path,
                'DataType': 'String'
            }
        
        if abricate_database:
            message_attributes['abricate_database'] = {
                'StringValue': abricate_database,
                'DataType': 'String'
            }
        queue.send_message(
            MessageBody = 'Running {0} workflow on {1}'.format(workflow, data_path),
            MessageAttributes = message_attributes
        )
        dynamodb_functions.add_workflow_submission_to_dynamodb(workflow, 's3://{0}'.format(data_path), current_user.email)

        response = {'status': 'submitted'}

    return response



