from boto3 import s3
from boto3 import Session

PROFILE_NAME = 'test'

session = Session(profile_name=PROFILE_NAME)
s3 = session.client('s3')

def get_s3_raw_data_paths(bucket = None):

    valid_run_dirs = []

    if bucket:
        buckets = [bucket]
    else:
        buckets = [bucket['Name'] for  bucket in s3.list_buckets()['Buckets']]
    for bucket in buckets:
        objects = s3.list_objects(Bucket = bucket, Prefix='runs/', Delimiter='/')
        if 'CommonPrefixes' in objects:
            for run_folder in objects['CommonPrefixes']:
                # print('{0}: {1}'.format(bucket, run_folder['Prefix']))
                objects = s3.list_objects(Bucket = bucket, Prefix=run_folder['Prefix'], Delimiter='/')
                if 'CommonPrefixes' in objects:
                    fastqs_dirs = [sub_dir['Prefix'] for
                        sub_dir in objects['CommonPrefixes']
                        if 'fastqs' in sub_dir['Prefix']
                    ]
                    if len(fastqs_dirs) == 1:
                        valid_run_dirs.append('{0}/{1}'.format(bucket,fastqs_dirs[0].replace('/fastqs/', '')))
    return valid_run_dirs

def get_s3_fasta_data_paths(bucket = None):

    valid_fasta_dirs = []

    if bucket:
        buckets = [bucket]
    else:
        buckets = [bucket['Name'] for  bucket in s3.list_buckets()['Buckets']]
    for bucket in buckets:
        objects = s3.list_objects(Bucket = bucket, Prefix='datasets/', Delimiter='/')
        if 'CommonPrefixes' in objects:
            for dataset_folder in objects['CommonPrefixes']:
                objects = s3.list_objects(Bucket = bucket, Prefix=dataset_folder['Prefix'], Delimiter='/')
                if 'CommonPrefixes' in objects:
                    fasta_dirs = [sub_dir['Prefix'] for
                        sub_dir in objects['CommonPrefixes']
                        if 'assemblies' in sub_dir['Prefix']
                    ]
                    if len(fasta_dirs) == 1:
                        valid_fasta_dirs.append('{0}/{1}'.format(bucket,fasta_dirs[0].replace('/assemblies/', '')))
    return valid_fasta_dirs


def get_s3_reference_paths(bucket = None):
    s3 = session.client('s3')
    reference_paths = []

    if bucket:
        buckets = [bucket]
    else:
        buckets = [bucket['Name'] for  bucket in s3.list_buckets()['Buckets']]
    for bucket in buckets:
        objects = s3.list_objects(Bucket = bucket, Prefix='references/', Delimiter='/')
        for reference in objects['Contents']:
            if '.' in reference['Key']: # if this is a file type key with an extension
                reference_paths.append('{0}/{1}'.format(bucket, reference['Key']))
    
    return reference_paths

