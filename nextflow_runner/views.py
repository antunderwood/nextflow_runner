from flask import render_template, request, redirect
from flask_security import login_required
from flask_login import current_user
from flask import jsonify
from nextflow_runner import job_submission
from nextflow_runner import app
from nextflow_runner import user_database
from nextflow_runner import dynamodb_functions
from nextflow_runner import template_helpers
from nextflow_runner import s3_functions

user_datastore = user_database.get_user_datastore()

# # Create a user to test with
# @app.before_first_request
# def create_user():
#     for Model in (Role, User, UserRoles):
#         Model.drop_table(fail_silently=True)
#         Model.create_table(fail_silently=True)
#     user_datastore.create_user(email='email2ants@gmail.com', password='password')


@app.route('/', methods = ['GET', 'POST'])
@login_required
def front_page():
    workflow_name = None
    data_path = None
    response = None
    if request.method == 'POST':
        workflow = request.form["workflow"]
        raw_data_path = request.form["raw_data_path"]
        fasta_data_path = request.form["fasta_data_path"]
        reference_path = request.form["reference_path"]
        abricate_database = request.form["abricate_database"]

        # determine datapath
        if workflow in ['assembly', 'snp_phylogeny']:
            data_path = raw_data_path
        elif workflow in ['abricate']:
            data_path = fasta_data_path

        # submit job based on workflow
        if workflow == 'snp_phylogeny':
            response = job_submission.submit_job_to_sqs(workflow, data_path, reference_path = reference_path)
        elif workflow == 'abricate':
            response = job_submission.submit_job_to_sqs(workflow, data_path, abricate_database = abricate_database)
        elif workflow == 'assembly':
            response = job_submission.submit_job_to_sqs(workflow, data_path)

        if response['status'] == 'submitted':
            return jsonify(status='notice', message='Workflow submitted')
        elif response['status'] == 'duplicated_submission':
            return jsonify(status='alert', message='Duplicate workflow: you have previously submitted this data to the {0} workflow'.format(request.form['workflow']))
    elif request.method == 'GET':
        return render_template('index.html',
                                response = response,
                                workflow_name = workflow_name,
                                data_path = data_path
                            )

@app.route('/get_previous_workflows')
@login_required
def workflows_table():
    user_workflows = dynamodb_functions.get_user_workflows(current_user.email)
    return render_template('workflows_table.html',
                            user_workflows = user_workflows,
                            prettyprint_isodate = template_helpers.prettyprint_isodate
                          )

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(front_page)

@app.route('/about', methods = ['GET'])
def about_page():
    return render_template('about.html')

@app.route('/_get_raw_data_paths')
@login_required
def _get_raw_data_paths():
    data_paths = s3_functions.get_s3_raw_data_paths('ghru-test-sequencing-runs')
    select_string = '<select id="raw_data_path" name="raw_data_path">'
    for data_path in data_paths:
        select_string += '<option value="{0}">{1}</option>'.format(data_path, data_path.split('/')[-1])
    select_string += '</select>'
    return select_string

@app.route('/_get_fasta_data_paths')
@login_required
def _get_fasta_data_paths():
    data_paths = s3_functions.get_s3_fasta_data_paths('ghru-test-assemblies')
    select_string = '<select id="fasta_data_path" name="fasta_data_path">'
    for data_path in data_paths:
        select_string += '<option value="{0}">{1}</option>'.format(data_path, data_path.split('/')[-1])
    select_string += '</select>'
    return select_string

@app.route('/_get_reference_paths')
@login_required
def _get_reference_paths():
    reference_paths = s3_functions.get_s3_reference_paths('ghru-test-reference-sequences')
    select_string = '<select id="reference_path" name="reference_path">'
    for reference_path in reference_paths:
        select_string += '<option value="{0}">{1}</option>'.format(reference_path, reference_path.split('/')[-1])
    select_string += '</select>'
    return select_string
            